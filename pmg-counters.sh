#!/bin/bash

# Variables
counters="bytes_in bytes_out count_in count_out spamcount_in spamcount_out viruscount_in viruscount_out pregreet_rejects rbl_rejects spfcount bounces_in bounces_out glcount count"

# Prepare
set -e
cd "$(dirname "${BASH_SOURCE[0]}")"
mkdir -p result
one_hour_ago=$(date -d '-1 hour' +%s)

# Get PMG counters
json=$(pmgsh get /statistics/mail --starttime=${one_hour_ago} || exit 1)

# Get counters
for counter_name in $counters; do
  counter_count=$(echo "${json}" | jq -r ".$counter_name")
  if [ "$counter_count" != "null" ]; then
    echo "$counter_count" > result/${counter_name}
  else
    echo "0" > result/${counter_name}
  fi
done
